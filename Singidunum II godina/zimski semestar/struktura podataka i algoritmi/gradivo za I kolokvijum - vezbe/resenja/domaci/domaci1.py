import random


def max_element_index(elements):
    """
    Funkcija koja pronalazi indeks elementa sa najvecom vrednoscu.
    Ukoliko ima vise elemenata sa najvecom vrednoscu, vraca indeks prvog najveceg elementa.
    :param elements: lista int-ova
    :return: indeks elementa sa najvecom vrednoscu
    """
    max_index = 0
    # Enumerate vraca parove (indeks, vrednost_elementa) za svaki element liste
    for i, value in enumerate(elements):
        if value > elements[max_index]:
            max_index = i
    return max_index


elements = []
for _ in range(50):
    elements.append(random.randint(0, 100))

indx = max_element_index(elements)
print(elements)
print("Indeks najveceg elementa je: ", indx)
