public class TestRedLista {

   public static void main(String[] args) {

      // Konstruisanje novog (praznog) reda
      RedLista r = new RedLista();
      System.out.println("Sadržaj reda: " + r);
      System.out.println("Veličina reda: " + r.dužina());
      System.out.println();

      // Dodavanje nekih elemenata u red
      int k = 17;
      r.enqueue(k);
      r.enqueue(23); r.enqueue(31); r.enqueue(47);
      r.enqueue(22); r.enqueue(11);
      System.out.println("Sadržaj reda: " + r);
      System.out.println("Veličina reda: " + r.dužina());
      System.out.println();

      // Uklanjanje nekih elemenata iz reda
      if (!r.prazanRed())
         System.out.println("Uklonjen element: " + r.dequeue());
      if (!r.prazanRed())
         System.out.println("Uklonjen element: " + r.dequeue());
      System.out.println("Sadržaj reda: " + r);
      System.out.println("Veličina reda: " + r.dužina());
      System.out.println();

      // Uzimanje vrednosti elementa na početku reda
      if (!r.prazanRed())
         System.out.println("Element na početku: " + r.peek());
      System.out.println("Veličina reda: " + r.dužina());
      System.out.println();

      // Još malo dodavanja i uklanjanja elemenata ...
      r.enqueue(69);
      r.dequeue(); r.dequeue(); r.peek();
      System.out.println("Sadržaj reda: " + r);
      System.out.println("Veličina reda: " + r.dužina());
      System.out.println();
   }
}

class RedLista {

   private ElementReda prvi, posl;  // pokazivači na prvi i poslednji element

   // Konstruktor za formiranje praznog reda
   public RedLista() {
      prvi = null;
      posl = null;
   }

   // Dužina reda
   public int dužina() {
       int n = 0;
       for (ElementReda elem = prvi; elem != null; elem = elem.sled) {
           n = n + 1;
       }
       return n;
   }

   // Ispitivanje da li je red prazan
   public boolean prazanRed() {
      return prvi == null;
   }

   // Dodavanje elementa sa ključem k na kraj reda
   public void enqueue(int k) {

      // Konstruisati novi element reda
      ElementReda elem = new ElementReda(k);

      if (prazanRed())
         prvi = elem;
      else
         posl.sled = elem;
      posl = elem;
   }

   // Uklanjanje elementa sa početka reda
   public int dequeue() {

      // Podrazumeva se da red nije prazan
   int k = prvi.ključ;
      if (posl == prvi)
         posl = null;
      prvi = prvi.sled;
   return k;
   }

   // Vraćanje vrednosti elementa sa početka reda
   public int peek() {
      // Podrazumeva se da red nije prazan
      return prvi.ključ;
   }

   // String reprezentacija reda
   public String toString() {

      String s = "";
      ElementReda elem = prvi;
      while (elem != null) {
         s = s + elem.ključ;
         elem = elem.sled;
         if (elem != null)
            s = s + ", ";
      }
      return s;
   }
}

class ElementReda {

   public int ključ;        // ključ elementa reda
   public ElementReda sled; // pokazivač na sledeći element reda

   // Konstruktor
   public ElementReda(int k) {
      ključ = k;
      sled = null;
   }
}