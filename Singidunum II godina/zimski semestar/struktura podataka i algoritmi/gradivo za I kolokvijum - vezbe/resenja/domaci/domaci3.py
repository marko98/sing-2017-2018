from time import time
from random import randint


def binarna_pretraga(lista, element):
    start_time = time()
    i = 0
    j = len(lista)-1
    while i <= j:
        # k = int((i+j)/2)
        k = (i + j) // 2  # // - je operator za celobrojno deljenje
        if element < lista[k]:
            j = k - 1
        elif element > lista[k]:
            i = k + 1
        else:
            return k, time() - start_time
    # pretpostavimo da je ovo nevalidan indeks
    # inace indeksiranje negativnim brojem znaci daj mi element na n-toj poziciji od pozadi
    # npr. lista[-1] je zapravo prvi element od kraja liste
    # ovo ce nam kasnije trebati kada budemo implementirali strukture podataka
    return -1, time() - start_time


def sekvencijalna_pretraga(lista, element):
    start_time = time()
    for i, elem in enumerate(lista):
        if elem == element:
            return i, time() - start_time
    return -1, time() - start_time

# Algoritam za sortiranje, potreban nam je kako bismo sortirali listu pre binarne pretrage
# Sortirana lista nije uslov za sekvencijalnu pretragu
def insertion_sort(list_of_elements):
    for i in range(1, len(list_of_elements)):
        j = i
        while (j > 0) and (list_of_elements[j] < list_of_elements[j-1]):
            list_of_elements[j], list_of_elements[j-1] = list_of_elements[j-1], list_of_elements[j]
            j -= 1

# Populisanje liste za pretragu
lista_elemenata = list()
for _ in range(100000):
    lista_elemenata.append(randint(1, 100000))

insertion_sort(lista_elemenata)
rez_bs = binarna_pretraga(lista_elemenata, 10)
rez_ss = sekvencijalna_pretraga(lista_elemenata, 10)

print("Element je pronadjen na poziciji: {}, binarnom pretragom, "
      "i trajalo je {} sekundi.".format(rez_bs[0], rez_bs[1]))
print("Element je pronadjen na poziciji: {}, sekvencijalnom pretragom, "
      "i trajalo je {} sekundi.".format(rez_ss[0], rez_ss[1]))
