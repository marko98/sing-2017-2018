// ,,Klijentska klasa'' za testiranje
public class TestPovezanaLista {

   public static void main(String[] args) {

      // Konstruisanje prazne liste
      PovezanaLista lista = new PovezanaLista();

      // Dodavanje nekih elemenata u listu
      int k = 17;
      lista.dodaj(k);
      lista.dodaj(23); lista.dodaj(31); lista.dodaj(47);
      lista.dodaj(k);

      // Prikazivanje elemenata liste
      System.out.println("Sadržaj liste:");
      System.out.println(lista);
      System.out.println("Dužina liste: " + lista.dužina());

      // Traženje elementa u listi
      k = 23;
      System.out.println();
      System.out.print("Element " + k + " se ");
      if (lista.nađi(k) == false)
         System.out.print("ne ");
      System.out.println("nalazi u listi");
   }
}

class PovezanaLista {

   private ElementListe prvi;   // pokazivač na prvi element liste
   private ElementListe posl;   // pokazivač na poslednji element liste
   private int n;               // broj elemenata liste

   // Konstruktor za formiranje prazne liste
   public PovezanaLista() {
      prvi = null;
      posl = null;
      n = 0;
   }

   // Dužina liste
   public int dužina() {
      return n;
   }

   // Ispitivanje da li je lista prazna
   public boolean praznaLista() {
      return prvi == null;
   }

   // Dodavanje elementa na kraj liste
   public void dodaj(int k) {

      // Konstruisati novi element liste
      ElementListe noviElem = new ElementListe(k);

      if (praznaLista())
         prvi = posl = noviElem;
      else {
         posl.sled = noviElem;
         posl = noviElem;
      }
      n = n + 1;
   }

   // Ispitivanje da li je dati element u listi
   public boolean nađi(int k) {

      ElementListe elem;
      for (elem = prvi; elem != null; elem = elem.sled) {
         if (elem.ključ == k)
            break;
      }
      return (elem != null);
   }

   // String reprezentacija povezane liste
   public String toString() {

      String s = "";
      for (ElementListe elem = prvi; elem != posl; elem = elem.sled) {
         s = s + elem.ključ + ", ";
      }
      if (posl != null)
         s = s + posl.ključ;
      return s;
   }
}

class ElementListe {

   public int ključ;         // sadržaj elementa liste
   public ElementListe sled; // pokazivač na sledeći element liste

   // Konstruktor
   public ElementListe(int k) {
      ključ = k;
      sled = null;
   }
}