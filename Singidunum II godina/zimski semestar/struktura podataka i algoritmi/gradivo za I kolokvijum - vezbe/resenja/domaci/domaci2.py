from time import time
from random import randint


def bubble_sort(list_of_elements):
    start_time = time()
    for i in range(len(list_of_elements)):
        for j in range(len(list_of_elements)-1, i, -1):
            if list_of_elements[j] < list_of_elements[j-1]:
                list_of_elements[j], list_of_elements[j-1] = list_of_elements[j-1], list_of_elements[j]
    return time() - start_time


def bubble_sort_2(list_of_elements):
    start_time = time()
    swapped = True
    while swapped is True:
        swapped = False
        for j in range(len(list_of_elements)-1, 0, -1):
            if list_of_elements[j] < list_of_elements[j-1]:
                list_of_elements[j], list_of_elements[j-1] = list_of_elements[j-1], list_of_elements[j]
                swapped = True

    return time() - start_time


def optimized_bubble_sort_2(list_of_elements):
    start_time = time()
    swapped = True
    while swapped is True:
        swapped = False
        i = 0
        for j in range(len(list_of_elements)-1, i, -1):
            if list_of_elements[j] < list_of_elements[j - 1]:
                list_of_elements[j], list_of_elements[j - 1] = list_of_elements[j - 1], list_of_elements[j]
                swapped = True
        i += 1

    return time() - start_time


def insertion_sort(list_of_elements):
    start_time = time()
    for i in range(1, len(list_of_elements)):
        j = i
        while (j > 0) and (list_of_elements[j] < list_of_elements[j-1]):
            list_of_elements[j], list_of_elements[j-1] = list_of_elements[j-1], list_of_elements[j]
            j -= 1

    return time() - start_time


def minr(list_of_elements, start_index, end_index):
    """
    minr - min right
    Algoritam min je dostupan u knjizi prof. Zivkovica str. 29-30. Ovo predstavlja specijalizaciju navedenog algoritma.
    Funkcija koja pronalazi indeks najmanjeg elementa u desnoj podlisti originalne liste
    :param list_of_elements: lista svih elemenata
    :param start_index: indeks od kojeg pocinje podlista
    :param end_index: indeks do kog ide podlista
    :return: indeks najmanjeg elementa desne podliste
    """
    # inicijalno najmanji element liste
    m = list_of_elements[start_index]
    # inicijalno indeks najmanjeg elementa liste
    j = start_index

    i = start_index+1
    # proveravamo sve ostale elemente niza
    while i < end_index:
        # ako je aktuelni element manji od najmanjeg
        if m > list_of_elements[i]:
            # zapamti vrednost novog najmanjeg elementa
            m = list_of_elements[i]
            # zapamti indeks novog najmanjeg elementa
            j = i

        # prelazimo na sledeci element niza
        i += 1

    # vracamo indeks najmanjeg elementa
    return j


def selection_sort(list_of_elements):
    start_time = time()
    for i in range(len(list_of_elements)):
        j = minr(list_of_elements, i, len(list_of_elements))
        list_of_elements[i], list_of_elements[j] = list_of_elements[j], list_of_elements[i]

    return time() - start_time


# -----------------------------------TESTIRANJE---------------------------------------
# Generisanje liste nasumicnih brojeva
lista = list()
lista1 = list()
lista2 = list()
lista3 = list()
lista4 = list()

for i in range(10000):
    random_broj = randint(1, 10000)
    lista.append(random_broj)
    lista1.append(random_broj)
    lista2.append(random_broj)
    lista3.append(random_broj)
    lista4.append(random_broj)


# Prikazivanje nesortirane liste
print("Pocetna lista: \n", lista)
# Pozivanje sortiranja liste sa bubble-sort algoritmom
print("Vreme potrebno za sortiranje algoritmom bubble-sort: \n", bubble_sort(lista))
# Pozivanje sortiranja liste sa bubble-sort-2 algoritmom
print("Vreme potrebno za sortiranje algoritmom bubble-sort-2: \n", bubble_sort_2(lista1))
# Pozivanje sortiranja liste sa optimized-bubble-sort-2 algoritmom
print("Vreme potrebno za sortiranje algoritmom optimized-bubble-sort-2: \n", optimized_bubble_sort_2(lista2))
# Pozivanje sortiranja liste sa insertion-sort algoritmom
print("Vreme potrebno za sortiranje algoritmom insertion-sort: \n", insertion_sort(lista3))
# Pozivanje sortiranja liste sa selection-sort algoritmom
print("Vreme potrebno za sortiranje algoritmom selection-sort: \n", selection_sort(lista4))
