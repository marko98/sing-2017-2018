// ,,Klijentska klasa'' za testiranje
public class TestStekLista {

   public static void main(String[] args) {

      // Konstruisanje novog (praznog) steka
      StekLista s = new StekLista();
      System.out.println("Sadržaj steka: " + s);
      System.out.println();

      // Dodavanje nekih elemenata na stek
      int k = 17;
      s.push(k);
      s.push(23); s.push(31); s.push(47);
      s.push(22); s.push(11);
      System.out.println("Sadržaj steka: " + s);
      System.out.println();

      // Uklanjanje nekih elemenata sa steka
      if (!s.prazanStek())
         System.out.println("Uklonjen element: " + s.pop());
      if (!s.prazanStek())
         System.out.println("Uklonjen element: " + s.pop());
      System.out.println("Sadržaj steka: " + s);
      System.out.println();

      // Uzimanje vrednosti elementa na vrhu steka
      if (!s.prazanStek())
         System.out.println("Element na vrhu: " + s.peek());
      System.out.println();

      // Još malo dodavanja i uklanjanja elemenata ...
      s.push(69);
      s.pop(); s.pop(); s.peek();
      System.out.println("Sadržaj steka: " + s);
      System.out.println();
   }
}

class StekLista {

   private ElementSteka vrh;   // pokazivač na vrh steka

   // Konstruktor za formiranje praznog steka
   public StekLista() {
      vrh = null;
   }

   // Ispitivanje da li je stek prazan
   public boolean prazanStek() {
      return vrh == null;
   }

   // Dodavanje elementa na vrh steka
   public void push(int k) {

      // Konstruisati novi element steka
      ElementSteka noviElem = new ElementSteka(k);

      if (prazanStek())
         vrh = noviElem;
      else {
         noviElem.sled = vrh;
         vrh = noviElem;
      }
   }

   // Uklanjanje elementa sa vrha steka
   public int pop() {

      // Podrazumeva se da stek nije prazan
      ElementSteka elem = vrh;
      vrh = elem.sled;
      return elem.ključ;
   }

   // Vraćanje vrednosti elementa na vrhu steka
   public int peek() {

      // Podrazumeva se da stek nije prazan
      return vrh.ključ;
   }

   // String reprezentacija steka
   public String toString() {

      String s = "";
      ElementSteka elem = vrh;
      while (elem != null) {
         s = s + elem.ključ;
         elem = elem.sled;
         if (elem != null)
            s = s + ", ";
      }
      return s;
   }
}

class ElementSteka {

   public int ključ;         // sadržaj elementa steka
   public ElementSteka sled; // pokazivač na sledeći element steka

   // Konstruktor
   public ElementSteka(int k) {
      ključ = k;
      sled = null;
   }
}