public class TestBSP {

   public static void main(String[] args) {

      BSP t = new BSP();  // prazno stablo

      t.dodaj(5);
      t.dodaj(11);
      t.dodaj(4);
      t.dodaj(15);
      t.dodaj(13);
      t.dodaj(16);
      t.dodaj(9);
      t.dodaj(2);
      t.dodaj(7);
      t.prikaži(); 
      System.out.println();

      if (t.nađi(10) == null)
         System.out.println("10 nije u stablu\n");
      else
         System.out.println("10 je u stablu\n");

      t.ukloni(11);
      t.prikaži(); 
      System.out.println();
   }
}

/******************** Reprezentacija BSP-a ********************/

class BSP {

   private Čvor koren;

   // Konstruktor
   public BSP() {

      koren = null;
   }

   /**************** Operacija nalaženja čvora ****************/

   // Nalaženje čvora sa ključem k
   public Čvor nađi(int k) {

      Čvor v = koren;
      while (v != null)
         if (k < v.ključ)
            v = v.levo;
         else if (k > v.ključ)
            v = v.desno;
         else // v je čvor sa ključem k
            break;
      return v;
   }

   /**************** Operacija dodavanja čvora ****************/

   // Dodavanje čvora sa ključem k
   public void dodaj(int k) {

      if (koren == null)
         koren = new Čvor(k);
      else {
         Čvor v = koren;
         while (true)
            if (k < v.ključ)
               if (v.levo == null) {
                  v.levo = new Čvor(k);
                  break;
               }
               else
                  v = v.levo;
            else if (k > v.ključ)
               if (v.desno == null) {
                  v.desno = new Čvor(k);
                  break;
               }
               else
                  v = v.desno;
            else // duplikat
               break;
      }
   }

   /**************** Operacija uklanjanja čvora ****************/

   // Uklanjanje čvora v
   private Čvor ukloni(Čvor v) {

      if (v.levo != null && v.desno != null) { // v ima dvoje dece
         Čvor w = v.desno; // w je najmanji čvor u desnom podstablu od v
         Čvor u = null;    // u je roditelj najmanjeg čvora w
         // Nalaženje najmanjeg čvora u desnom podstablu čvora v
         while (w.levo != null) {
            u = w;
            w = w.levo;
         }
         // Zamenjivanje ključa čvora v ključem najmanjeg čvora w
         v.ključ = w.ključ;
         // Uklanjanje najmanjeg čvora w u desnom podstablu čvora v
         if (u == null)  // v je roditelj od w
            v.desno = w.desno;
         else
            u.levo = w.desno;
      }
      else  // v ima najviše jedno dete
         if (v.levo != null )
            v = v.levo;
         else
            v = v.desno;
      return v;
   }

   // Uklanjanje čvora sa ključem k
   public void ukloni(int k) {

      Čvor v = koren;
      while (v != null) {
         if (k < v.ključ)
            if (v.levo != null && v.levo.ključ == k)
               v.levo = ukloni(v.levo);
            else
               v = v.levo;
         else if (k == v.ključ) {
            koren = ukloni(v);
            break;
         }
         else // k > v.ključ
            if (v.desno != null && v.desno.ključ == k)
               v.desno = ukloni(v.desno);
            else
               v = v.desno;
      }
   }

   /************** Operacija prikazivanja stabla **************/

   // Prikazivanje stabla na ekranu
   public void prikaži() {
     if(koren == null )
        System.out.println("Prazno stablo");
     else
        prikaži(koren);
   }
   private void prikaži(Čvor v) {
      if (v != null) {
          prikaži(v.levo);
          v.prikaži();
          prikaži(v.desno);
      }
   }
}

/************* Reprezentacija jednog čvora BSP-a **************/

class Čvor {

   public int  ključ;  // Comparable
   public Čvor levo;
   public Čvor desno;

   // Konstruktor
   public Čvor(int ključ) {
         this.ključ = ključ;
         this.levo = null;
         this.desno = null;
   }

   // Prikazivanje ključa čvora
   public void prikaži() {
      System.out.println(ključ);
   }
}
